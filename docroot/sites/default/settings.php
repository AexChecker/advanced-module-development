<?php

// @codingStandardsIgnoreFile

$settings['hash_salt'] = 'jaLUvgKSv8e4OuNGmx1T8j-20IFLfXP7i2zKmEY-YCxw7SzepqtfbmL7yspMw6FQsQ7nWRPNAg';

$settings['update_free_access'] = FALSE;
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';

$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

$settings['entity_update_batch_size'] = 50;
$settings['entity_update_backup'] = TRUE;
$settings['file_chmod_directory'] = 0775;
$settings['file_chmod_file'] = 0664;
$settings['file_public_path'] = 'sites/default/files';
$settings['file_private_path'] = '../files-private';
$config['system.file']['path']['temporary'] = '/tmp';

if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}

$databases['default']['default'] = [
  'database' => getenv('DB_NAME'),
  'username' => getenv('DB_USER'),
  'password' => getenv('DB_PASSWORD'),
  'host' => getenv('DB_HOST'),
  'driver' => getenv('DB_DRIVER'),
  'port' => '3306',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
];

$settings['config_sync_directory'] = '../config/default';

//require DRUPAL_ROOT . "/../vendor/acquia/blt/settings/blt.settings.php";

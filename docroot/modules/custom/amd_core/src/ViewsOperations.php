<?php

namespace Drupal\amd_core;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ViewsExposedForms class to handle Views functionality.
 */
class ViewsOperations implements ContainerInjectionInterface {

  use StringInflectorTrait;

  /**
   * The Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ViewsExposedForms constructor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The global Drupal container.
   */
  public function __construct(ContainerInterface $container) {
    $this->entityTypeManager = $container->get('entity_type.manager');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container);
  }

  /**
   * Views exposed form.
   *
   * @param array $form
   *   A form structure array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   A form state object.
   *
   * @see hook_form_FORM_ID_alter()
   */
  public function viewsExposedFormAlter(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\views\ViewExecutable $view */
    $view = $form_state->getStorage()['view'];

    $this->invokeInflectorMethods($view, __FUNCTION__, [&$form, $form_state]);
  }

  /**
   * Views query alter.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   A view object.
   * @param \Drupal\views\Plugin\views\query\QueryPluginBase $query
   *   A query object.
   *
   * @see hook_views_query_alter()
   */
  public function viewsQueryAlter(ViewExecutable $view, QueryPluginBase $query) {
    $this->invokeInflectorMethods($view, __FUNCTION__, [$view, $query]);
    $this->invokeInflectorMethods($query, __FUNCTION__, [$view, $query]);
  }

  /**
   * Initialize hook_pre_render() functionality.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   A view object.
   */
  public function viewsPreRender(ViewExecutable $view) {
    $this->invokeInflectorMethods($view, __FUNCTION__, [$view]);
  }

  /**
   * Views query alter for "content".
   *
   * @param \Drupal\views\ViewExecutable $view
   *   A view object.
   * @param \Drupal\views\Plugin\views\query\QueryPluginBase $query
   *   A query object.
   *
   * @see self::viewsQueryAlter()
   * @see hook_views_query_alter()
   */
  protected function viewsQueryAlterForContent(ViewExecutable $view, QueryPluginBase $query) {
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    if (isset($query->relationships['content_moderation_state_field_data_node_field_data'])) {
      $conditions_kvp = [
        'langcode' => 'langcode',
        'vid' => 'content_entity_revision_id',
      ];

      foreach ($conditions_kvp as $k => $v) {
        $query->addWhereExpression(0, "node_field_data.{$k} = content_moderation_state_field_data_node_field_data.{$v}");
      }
    }
  }

  /**
   * Alters the result for content_listing view.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   A view object.
   */
  protected function viewsPreRenderForContentListing(ViewExecutable $view) {
    // Special case where we need to swap the moderated timestamp
    // in favor of the created timestamp. The editors are given
    // the ability to control the creation date at will, and this
    // makes sure to display the created date for specific CT.
    foreach ($view->result as $row) {
      if (!in_array($row->_entity->bundle(), ['news', 'reference'])) {
        continue;
      }

      if (!empty($row->node_field_data_created)) {
        $row->content_moderation_state_field_data_node_field_data__content = $row->node_field_data_created;
      }
    }
  }

}

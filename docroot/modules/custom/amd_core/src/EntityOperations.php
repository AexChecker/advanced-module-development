<?php

namespace Drupal\amd_core;

use Drupal\media\MediaInterface;
use Drupal\menu_link_content\MenuLinkContentInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for reacting to entity events.
 */
class EntityOperations implements ContainerInjectionInterface {

  use StringInflectorTrait;

  /**
   * The Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Entity Field Manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The Logger Channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The Cache Tags Invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidator
   */
  protected $cacheTagsInvalidator;

  /**
   * Lightweight map of fields across entities.
   *
   * @var array
   */
  protected $contentFieldMap;

  /**
   * Lightweight field type list.
   *
   * @var array
   */
  protected $contentFieldTypes = [
    'entity_reference',
    'entity_reference_revisions',
  ];

  /**
   * Menu cache bin (Menus/Links).
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $menuCacheBackend;

  /**
   * Discovery cache bin (Actions/Tasks).
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $discoveryCacheBackend;

  /**
   * Render cache bin (Pages/Blocks).
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $renderCacheBackend;

  /**
   * EntityOperations constructor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The global Drupal container.
   */
  public function __construct(ContainerInterface $container) {
    $this->entityTypeManager = $container->get('entity_type.manager');
    $this->entityFieldManager = $container->get('entity_field.manager');
    $this->logger = $container->get('logger.factory')->get('amd_core');
    $this->cacheTagsInvalidator = $container->get('cache_tags.invalidator');
    $this->menuCacheBackend = $container->get('cache.menu');
    $this->discoveryCacheBackend = $container->get('cache.discovery');
    $this->renderCacheBackend = $container->get('cache.render');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container);
  }

  /**
   * Acts on an entity and set published status based on the moderation state.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being saved.
   *
   * @see hook_entity_presave()
   */
  public function entityPreSave(EntityInterface $entity) {
    $this->invokeInflectorMethods($entity, 'entityPreSave', [$entity]);
  }

  public function entityPreSaveNodePage(EntityInterface $entity) {
    $entity->setRevisionLogMessage(__METHOD__);
    $this->doSomthing($entity);
  }

  public function entityPreSaveNodeLandingPage(EntityInterface $entity) {
    $entity->setRevisionLogMessage(__METHOD__);
    $this->doSomthing($entity);
  }

  private function doSomthing($entity) {

  }

}

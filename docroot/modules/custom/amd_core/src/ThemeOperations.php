<?php

namespace Drupal\amd_core;

use Drupal\node\Entity\Node;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for entity themes.
 */
class ThemeOperations implements ContainerInjectionInterface {

  use StringInflectorTrait;

  /**
   * The Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Logger Channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The currently active route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * ThemeOperations constructor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The global Drupal container.
   */
  public function __construct(ContainerInterface $container) {
    $this->entityTypeManager = $container->get('entity_type.manager');
    $this->logger = $container->get('logger.factory')->get('amd_core');
    $this->currentRouteMatch = $container->get('current_route_match');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container);
  }

  /**
   * Perform alterations for "amd_core_preprocess_video_embed_html5()".
   *
   * @param array $variables
   *   A theme variables.
   */
  public function preprocessVideoEmbedHtml5(array &$variables) {
    $variables += [
      'controls' => TRUE,
      'loop' => FALSE,
      'muted' => FALSE,
      'playsinline' => FALSE,
    ];

    if ($this->currentRouteMatch->getRouteName() !== 'entity.node.canonical') {
      return;
    }

    /** @var \Drupal\node\Entity\Node $node */
    if (!($node = $this->currentRouteMatch->getParameter('node'))) {
      return;
    }

    $this->invokeInflectorMethods('for_hero_section', __FUNCTION__, [
      &$variables,
      $node,
    ]);
  }

  /**
   * Perform alterations for "static::preprocessVideoEmbedHtml5()".
   *
   * @param array $variables
   *   A theme variables.
   * @param \Drupal\node\Entity\Node $node
   *   The Node entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function preprocessVideoEmbedHtml5ForHeroSection(array &$variables, Node $node) {
    if ($node->get('field_hero_section')->isEmpty()) {
      return;
    }

    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $this->entityTypeManager
      ->getStorage('paragraph')
      ->load($node->get('field_hero_section')->target_id);

    if (!$paragraph || $paragraph->bundle() !== 'hero_video') {
      return;
    }
    if ($paragraph->get('field_video')->isEmpty()) {
      return;
    }

    /** @var \Drupal\media\Entity\Media $media */
    $media = $this->entityTypeManager
      ->getStorage('media')
      ->load($paragraph->get('field_video')->target_id);

    if (!$media || $media->get('field_video_type')->getString() !== 'file') {
      return;
    }

    if ($media->get('field_video_file')->isEmpty()) {
      return;
    }

    /** @var \Drupal\file\Entity\File $file */
    $file = $this->entityTypeManager
      ->getStorage('file')
      ->load($media->get('field_video_file')->target_id);

    if (!$file) {
      return;
    }

    // We need to be sure that we have exactly required video file to override
    // default properties for the template.
    $file_name = str_replace('%20', ' ', basename($variables['src']));
    if (basename($file->get('uri')->getString()) !== $file_name) {
      return;
    }

    $variables['autoplay'] = TRUE;
    $variables['controls'] = FALSE;
    $variables['loop'] = TRUE;
    $variables['muted'] = TRUE;
  }

}
